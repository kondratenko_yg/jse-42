package ru.kondratenko.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kondratenko.tm.enumerated.Status;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListTaskResponseDTO {
    private Status status;
    private TaskDTO[] payloadTask;
}
