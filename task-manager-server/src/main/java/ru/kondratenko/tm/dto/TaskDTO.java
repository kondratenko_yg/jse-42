package ru.kondratenko.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kondratenko.tm.entity.AbstractEntity;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskDTO extends AbstractEntity {
    public static final Long serialVersionUID = 1L;

    private Long id;

    private String name;

    private String description;

    private Long projectId;

    private Long userId;

    private String deadline;
}

