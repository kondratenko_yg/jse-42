package ru.kondratenko.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.enumerated.Status;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListProjectResponseDTO {
    private Status status;
    private Project[] payloadProject;
}
