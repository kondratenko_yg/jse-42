package ru.kondratenko.tm.service;

import ru.kondratenko.tm.dto.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.ProjectResponseDTO;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.enumerated.Status;
import ru.kondratenko.tm.repository.ProjectRepository;
import ru.kondratenko.tm.util.Helper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public  class ProjectService implements IProjectIService {

    private static volatile ProjectService instance = null;

    private final ProjectRepository projectRepository = ProjectRepository.getInstance();

    private ProjectService() {
    }

    public static ProjectService getInstance() {
        ProjectService localInstance = instance;
        if (localInstance == null) {
            synchronized (ProjectService.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ProjectService();
                }
            }
        }
        return localInstance;
    }

    @Override
    public ProjectResponseDTO create(final Project project) {
        String name = project.getName();
        if(name.equals("") || Helper.checkProjectName(name) || name.isEmpty()){
            return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Optional<Project> projectOptional = projectRepository.create(project);
        if (projectOptional.isPresent()) {
            return ProjectResponseDTO.builder().payloadProject(projectOptional.get()).status(Status.OK).build();
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ProjectResponseDTO updateByIndex(final int index, Project project)  {
        Optional<Project> project1 = projectRepository.findByIndex(index);
        if(project1.isPresent()){
            Project updatedProject = Project.builder()
                    .id(project1.get().getId())
                    .name(project.getName())
                    .description(project.getDescription())
                    .userId(project.getUserId()).build();
            project1 = projectRepository.update(updatedProject);
            if(project1.isPresent()) {
                return ProjectResponseDTO.builder().payloadProject(project1.get()).status(Status.OK).build();
            }
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ProjectResponseDTO updateById(final long id, Project project){
        Optional<Project> project1 = projectRepository.findById(id);
        if(project1.isPresent()){
            Project updatedProject = Project.builder()
                    .id(project1.get().getId())
                    .name(project.getName())
                    .description(project.getDescription())
                    .userId(project.getUserId()).build();
            project1 = projectRepository.update(updatedProject);
            if(project1.isPresent()) {
                return ProjectResponseDTO.builder().payloadProject(project1.get()).status(Status.OK).build();
            }
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
    }


    @Override
    public ProjectResponseDTO findByIndex(final int index)  {
        Optional<Project> project1 = projectRepository.findByIndex(index);
        if(project1.isPresent()) {
            return ProjectResponseDTO.builder().payloadProject(project1.get()).status(Status.OK).build();
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListProjectResponseDTO findByName(final String name)  {
        List<Project> project1 = projectRepository.findByName(name);
        if(project1.isEmpty()) {
            return ListProjectResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        return ListProjectResponseDTO
                .builder()
                .payloadProject(project1.toArray(new Project[0]))
                .status(Status.OK).build();
    }

    @Override
    public ProjectResponseDTO findById(final Long id) {
        Optional<Project> project1 = projectRepository.findById(id);
        if(project1.isPresent()) {
            return ProjectResponseDTO.builder().payloadProject(project1.get()).status(Status.OK).build();
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ProjectResponseDTO removeByIndex(final Integer index)  {
        projectRepository.removeByIndex(index);
        return ProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ProjectResponseDTO removeById(final Long id)  {
        projectRepository.removeById(id);
        return ProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ProjectResponseDTO removeByName(final String name)  {
        projectRepository.removeByName(name);
        return ProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public ListProjectResponseDTO findAll() {
        return ListProjectResponseDTO
                .builder()
                .payloadProject(projectRepository.findAll().toArray(new Project[0]))
                .status(Status.OK).build();
    }

    @Override
    public ListProjectResponseDTO findAllByUserId(Long userId) {
        if (userId == null) return ListProjectResponseDTO.builder().status(Status.DB_ERROR).build();
        return  ListProjectResponseDTO
                .builder()
                .payloadProject(projectRepository.findAllByUserId(userId).toArray(new Project[0]))
                .status(Status.OK).build();
    }

    @Override
    public ListProjectResponseDTO saveJSON(final String  fileName) throws IOException {
        writeJSON(fileName,projectRepository.findAll());
        return ListProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListProjectResponseDTO saveXML(final String fileName) throws IOException {
        writeXML(fileName,projectRepository.findAll());
        return ListProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListProjectResponseDTO uploadFromJSON(final String  fileName) throws IOException {
        List<Project> projects = uploadJSONToList(fileName,Project.class);
        clear();
        projects.forEach(this::create);
        return ListProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListProjectResponseDTO uploadFromXML(final String  fileName) throws IOException {
        List<Project> projects = uploadXMLToList(fileName,Project.class);
        clear();
        projects.forEach(this::create);
        return ListProjectResponseDTO.builder().status(Status.OK).build();
    }
}
