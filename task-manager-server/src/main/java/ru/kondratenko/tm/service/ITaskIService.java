package ru.kondratenko.tm.service;

import ru.kondratenko.tm.dto.ListTaskResponseDTO;
import ru.kondratenko.tm.dto.TaskDTO;
import ru.kondratenko.tm.dto.TaskResponseDTO;
import ru.kondratenko.tm.exception.TaskNotFoundException;

public interface ITaskIService extends IService<TaskDTO, TaskResponseDTO, ListTaskResponseDTO> {
    ListTaskResponseDTO findAllByProjectId(final Long projectId);
    TaskResponseDTO findByProjectIdAndId(final Long projectId, final Long id);
    ListTaskResponseDTO findAllByUserId(Long Id);
    ListTaskResponseDTO findByName(final String name) throws TaskNotFoundException;
    TaskResponseDTO removeByName(final String name) throws TaskNotFoundException;
}
