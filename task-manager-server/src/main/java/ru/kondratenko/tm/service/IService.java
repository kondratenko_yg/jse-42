package ru.kondratenko.tm.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.kondratenko.tm.exception.NotFoundException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public interface IService<T,E,L> {
    E create(final T item);
    E updateById(final long id, T item) throws NotFoundException;
    E updateByIndex(final int index, T item) throws NotFoundException;
    E findByIndex(final int index) throws NotFoundException;
    E findById(final Long id) throws NotFoundException;
    E removeById(final Long id) throws NotFoundException;
    E removeByIndex(final Integer index) throws NotFoundException;

    void clear();
    L findAll();

    L saveJSON(final String  fileName) throws IOException;
    L saveXML(final String  fileName) throws IOException;
    L uploadFromJSON(final String  fileName) throws IOException;
    L uploadFromXML(final String  fileName) throws IOException;

    default int writeJSON(String fileName, List<T> items) throws IOException {
        final File file = new File(fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.writeValue(file, items);
        return 0;
    }

    default int writeXML(String fileName, List<T> items) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writeValue(new File(fileName), items);
        return 0;
    }

    default List<T> uploadJSONToList(String fileName, Class<T> clazz) throws IOException {
        final File file = new File(fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        clear();
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        JavaType type = typeFactory.constructCollectionType(List.class, clazz);
        JavaType typeString = typeFactory.constructType(String.class);
        MapType mapType = typeFactory.constructMapType(HashMap.class, typeString, type);
        return objectMapper.readValue(file, type);
    }

    default List<T> uploadXMLToList(String fileName, Class<T> clazz) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        JavaType type = xmlMapper.getTypeFactory().constructCollectionType(List.class, clazz);
        JavaType typeString = xmlMapper.getTypeFactory().constructType(String.class);
        MapType mapType = xmlMapper.getTypeFactory().constructMapType(HashMap.class, typeString, type);
        return xmlMapper.readValue(new File(fileName), type);
    }
}
