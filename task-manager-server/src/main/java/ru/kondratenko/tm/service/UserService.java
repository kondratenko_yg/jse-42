package ru.kondratenko.tm.service;

import ru.kondratenko.tm.dto.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.ListUserResponseDTO;
import ru.kondratenko.tm.dto.UserResponseDTO;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.enumerated.Status;
import ru.kondratenko.tm.repository.UserRepository;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static ru.kondratenko.tm.util.HashUtil.hashMD5;

public class UserService implements IUserIService {

    private final UserRepository userRepository;

    private static volatile UserService instance;

    private UserService() {
        this.userRepository = UserRepository.getInstance();
    }

    public static UserService getInstance() {
        UserService localInstance = instance;
        if (localInstance == null) {
            synchronized (UserService.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new UserService();
                }
            }
        }
        return localInstance;
    }

    @Override
    public UserResponseDTO create(final User user) {
        final String login = user.getName(), password = user.getPassword();
        Role role = user.getRole();
        if (login == null || login.isEmpty() || password == null || password.isEmpty()) {
            return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Optional<User> userOptional = userRepository.findByName(login);
        if (userOptional.isPresent()) {
            return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        userOptional = userRepository.create(User.builder()
                .id(user.getId())
                .name(login)
                .password(hashMD5(password))
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .role(role == null ? Role.USER : role).build());
        if (userOptional.isPresent()) {
            return UserResponseDTO.builder().payloadUser(userOptional.get()).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO updateByIndex(final int index, final User user) {
        final String login = user.getName();
        final String password = user.getPassword();
        if (login == null || login.isEmpty() || index < 0) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        if (password == null) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<User> user1 = userRepository.findByIndex(index);
        if (user1.isPresent()) {
            User userUpdate = User.builder()
                    .id(user1.get().getId())
                    .name(login)
                    .password(hashMD5(password))
                    .firstName(user.getFirstName())
                    .lastName(user.getLastName())
                    .build();
            user1 = userRepository.update(userUpdate);
            if (user1.isPresent()) {
                return UserResponseDTO.builder().payloadUser(user1.get()).status(Status.OK).build();
            }
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO updateById(final long id, final User user) {
        final String login = user.getName();
        final String password = user.getPassword();
        if (login == null || login.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        if (password == null) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<User> user1 = userRepository.findById(id);
        if (user1.isPresent()) {
            User userUpdate = User.builder()
                    .id(user1.get().getId())
                    .name(login)
                    .password(hashMD5(password))
                    .firstName(user.getFirstName())
                    .lastName(user.getLastName())
                    .build();
            user1 = userRepository.update(userUpdate);
            if (user1.isPresent()) {
                return UserResponseDTO.builder().payloadUser(user1.get()).status(Status.OK).build();
            }
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO updateByName(final String name, final User user) {
        final String login = user.getName();
        final String password = user.getPassword();
        if (login == null || login.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        if (password == null) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<User> user1 = userRepository.findByName(name);
        if (user1.isPresent()) {
            User userUpdate = User.builder()
                    .id(user1.get().getId())
                    .name(login)
                    .password(hashMD5(password))
                    .firstName(user.getFirstName())
                    .lastName(user.getLastName())
                    .build();
            user1 = userRepository.update(userUpdate);
            if (user1.isPresent()) {
                return UserResponseDTO.builder().payloadUser(user1.get()).status(Status.OK).build();
            }
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public boolean checkPassword(final Optional<User> user, final String password) {
        return user.map(value -> value.getPassword().equals(hashMD5(password))).orElse(false);
    }

    @Override
    public UserResponseDTO findByName(String login) {
        if (login == null || login.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<User> user = userRepository.findByName(login);
        if (user.isPresent()) {
            return UserResponseDTO.builder().payloadUser(user.get()).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO findById(Long id) {
        if (id == null) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            return UserResponseDTO.builder().payloadUser(user.get()).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO findByIndex(final int index) {
        if (index < 0) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<User> user = userRepository.findByIndex(index);
        if (user.isPresent()) {
            return UserResponseDTO.builder().payloadUser(user.get()).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO removeByName(String login) {
        if (login == null || login.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        userRepository.removeByName(login);
        return UserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public UserResponseDTO removeById(Long id) {
        if (id == null) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        userRepository.removeById(id);
        return UserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public UserResponseDTO removeByIndex(Integer index) {
        if (index < 0) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        userRepository.removeByIndex(index);
        return UserResponseDTO.builder().status(Status.OK).build();

    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public ListUserResponseDTO findAll() {
        return ListUserResponseDTO
                .builder()
                .payloadUser(userRepository.findAll().toArray(new User[0]))
                .status(Status.OK).build();
    }

    @Override
    public ListUserResponseDTO saveJSON(final String  fileName) throws IOException {
        writeJSON(fileName,userRepository.findAll());
        return ListUserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListUserResponseDTO saveXML(final String fileName) throws IOException {
        writeXML(fileName,userRepository.findAll());
        return ListUserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListUserResponseDTO uploadFromJSON(final String  fileName) throws IOException {
        List<User> users = uploadJSONToList(fileName,User.class);
        clear();
        users.forEach(this::create);
        return ListUserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListUserResponseDTO uploadFromXML(final String  fileName) throws IOException {
        List<User> users = uploadXMLToList(fileName,User.class);
        clear();
        users.forEach(this::create);
        return ListUserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public User getCurrentUser() {
        return userRepository.currentUser;
    }

    @Override
    public void setCurrentUser(User user) {
        userRepository.currentUser = user;
    }


}
