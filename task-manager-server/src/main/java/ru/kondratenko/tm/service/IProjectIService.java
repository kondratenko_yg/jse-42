package ru.kondratenko.tm.service;

import ru.kondratenko.tm.dto.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.ProjectResponseDTO;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.exception.NotFoundException;

public interface IProjectIService extends IService<Project, ProjectResponseDTO, ListProjectResponseDTO> {
    ListProjectResponseDTO findAllByUserId(Long Id);
    ListProjectResponseDTO findByName(final String name) throws NotFoundException;
    ProjectResponseDTO removeByName(final String name) throws NotFoundException;
}
