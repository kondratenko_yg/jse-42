package ru.kondratenko.tm.service;


import ru.kondratenko.tm.dto.ListTaskResponseDTO;
import ru.kondratenko.tm.dto.TaskResponseDTO;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;

public interface IProjectTaskService {
    TaskResponseDTO addTaskToProject(final Long projectId, final Long taskId) throws ProjectNotFoundException, TaskNotFoundException;
    TaskResponseDTO removeTaskFromProject(final Long projectId, final Long taskId);
    void clear();
    ListTaskResponseDTO findAllByProjectId(Long projectId);
}
