package ru.kondratenko.tm.controller;

import lombok.var;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.kondratenko.tm.dto.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.ProjectResponseDTO;
import ru.kondratenko.tm.dto.TaskDTO;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.enumerated.Status;
import ru.kondratenko.tm.service.ProjectService;
import ru.kondratenko.tm.service.ProjectTaskService;
import ru.kondratenko.tm.service.UserService;

import javax.jws.WebService;
import java.io.IOException;
import java.util.Arrays;

@WebService(endpointInterface = "ru.kondratenko.tm.controller.IControllerProject")
public class IControllerProjectImpl implements IControllerProject {
    private final ProjectService projectService;
    private final UserService userService;
    private final ProjectTaskService projectTaskService;
    protected final Logger logger;

    public IControllerProjectImpl() {
        this.projectService = ProjectService.getInstance();
        this.userService = UserService.getInstance();
        this.projectTaskService = ProjectTaskService.getInstance();
        this.logger = LogManager.getLogger(IControllerProjectImpl.class);
    }

    @Override
    public ProjectResponseDTO create(final Project project) {
        if (userService.getCurrentUser() != null) {
            project.setUserId(userService.getCurrentUser().getId());
        }
        return projectService.create(project);
    }

    @Override
    public ProjectResponseDTO updateByIndex(final int index, Project project) {
        return projectService.updateByIndex(index, project);
    }

    @Override
    public ProjectResponseDTO updateById(final long id, Project project) {
        return projectService.updateById(id, project);
    }

    @Override
    public ProjectResponseDTO clear() {
        if (userService.getCurrentUser() == null) {
            projectService.clear();
            projectTaskService.clear();
        } else {
            for (Project project : projectService.findAllByUserId(userService.getCurrentUser().getId()).getPayloadProject()) {
                projectService.removeById(project.getId());
            }
        }
        return ProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ProjectResponseDTO removeByName(final String name) {
        final var projects = projectService.findByName(name);
        if (projects.getStatus() == Status.OK) {
            final var responseDTO = projectService.removeByName(name);
            if (responseDTO.getStatus() == Status.OK) {
                Arrays.stream(projects.getPayloadProject()).forEach(project -> {
                    final var tasks = projectTaskService.findAllByProjectId(project.getId());
                    if (tasks.getStatus() == Status.OK) {
                        for (final TaskDTO task : tasks.getPayloadTask()) {
                            projectTaskService.removeTaskFromProject(project.getId(), task.getId());
                        }
                    }
                });
            }
            return responseDTO;
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ProjectResponseDTO removeById(final long id) {
        final var response = projectService.removeById(id);
        if (response.getStatus() == Status.OK) {
            final var tasks = projectTaskService.findAllByProjectId(id);
            if (tasks.getStatus() == Status.OK) {
                for (final TaskDTO task : tasks.getPayloadTask()) {
                    projectTaskService.removeTaskFromProject(id, task.getId());
                }
            }
        }
        return response;
    }

    @Override
    public ProjectResponseDTO removeByIndex(final int index) {
        var project = projectService.findByIndex(index);
        var response = projectService.removeByIndex(index);
        if (response.getStatus() == Status.OK) {
            final var tasks = projectTaskService.findAllByProjectId(project.getPayloadProject().getId());
            if (tasks.getStatus() == Status.OK) {
                for (final TaskDTO task : tasks.getPayloadTask()) {
                    projectTaskService.removeTaskFromProject(project.getPayloadProject().getId(), task.getId());
                }
            }
        }
        return response;
    }

    @Override
    public ProjectResponseDTO viewById(final Long id) {
        return projectService.findById(id);
    }

    @Override
    public ProjectResponseDTO viewByIndex(final int index) {
        return projectService.findByIndex(index);
    }

    @Override
    public ListProjectResponseDTO viewByName(final String name) {
        return projectService.findByName(name);
    }

    @Override
    public ListProjectResponseDTO list() {
        ListProjectResponseDTO projectList;
        if (userService.getCurrentUser() == null) {
            projectList = projectService.findAll();
        } else {
            projectList = projectService.findAllByUserId(userService.getCurrentUser().getId());
        }
        return projectList;

    }

    @Override
    public ProjectResponseDTO saveJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
        projectService.saveJSON(fileName);
        return ProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ProjectResponseDTO saveXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
        projectService.saveXML(fileName);
        return ProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ProjectResponseDTO uploadFromJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
        projectService.uploadFromJSON(fileName);
        return ProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ProjectResponseDTO uploadFromXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
        projectService.uploadFromXML(fileName);
        return ProjectResponseDTO.builder().status(Status.OK).build();
    }
}
