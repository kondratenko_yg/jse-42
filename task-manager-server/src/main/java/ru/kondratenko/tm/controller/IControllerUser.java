package ru.kondratenko.tm.controller;

import ru.kondratenko.tm.dto.ListUserResponseDTO;
import ru.kondratenko.tm.dto.UserResponseDTO;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.exception.NotFoundException;
import ru.kondratenko.tm.exception.UserNotFoundException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.IOException;

@WebService
@SOAPBinding(
        style = SOAPBinding.Style.RPC
)
public interface IControllerUser {
    @WebMethod
    UserResponseDTO create(final User user);

    @WebMethod
    UserResponseDTO viewByName(@WebParam(name = "name") final String name) throws UserNotFoundException;

    @WebMethod
    UserResponseDTO updateByIndex(@WebParam(name = "index") final int index, User user) throws UserNotFoundException;

    @WebMethod
    UserResponseDTO updateById(@WebParam(name = "id") final long id, User user) throws UserNotFoundException;

    @WebMethod
    UserResponseDTO updateByName(@WebParam(name = "name") final String name, User user) throws UserNotFoundException;

    @WebMethod
    UserResponseDTO registry(@WebParam(name = "name") final String name, @WebParam(name = "password") String password) throws UserNotFoundException;

    @WebMethod
    UserResponseDTO logOff();

    @WebMethod
    UserResponseDTO displayUserInfo() throws UserNotFoundException;

    @WebMethod
    UserResponseDTO viewById( @WebParam(name = "id") final Long id) throws NotFoundException;

    @WebMethod
    UserResponseDTO viewByIndex(@WebParam(name = "index") final int index) throws NotFoundException;

    @WebMethod
    UserResponseDTO removeByIndex(@WebParam(name = "index") final int index) throws NotFoundException;

    @WebMethod
    UserResponseDTO removeById(@WebParam(name = "id") final long id) throws NotFoundException;

    @WebMethod
    UserResponseDTO removeByName(@WebParam(name = "name") final String name) throws NotFoundException;

    @WebMethod
    UserResponseDTO clear() throws NotFoundException;

    @WebMethod
    ListUserResponseDTO list();

    @WebMethod
    UserResponseDTO saveJSON(@WebParam(name = "fileName") final String fileName) throws IOException;

    @WebMethod
    UserResponseDTO saveXML(@WebParam(name = "fileName") final String fileName) throws IOException;

    @WebMethod
    UserResponseDTO uploadFromJSON(@WebParam(name = "fileName") final String fileName) throws IOException;

    @WebMethod
    UserResponseDTO uploadFromXML(@WebParam(name = "fileName") final String fileName) throws IOException;
}
