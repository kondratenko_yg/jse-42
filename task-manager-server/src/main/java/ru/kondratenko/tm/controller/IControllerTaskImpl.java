package ru.kondratenko.tm.controller;

import ru.kondratenko.tm.dto.ListTaskResponseDTO;
import ru.kondratenko.tm.dto.TaskDTO;
import ru.kondratenko.tm.dto.TaskResponseDTO;
import ru.kondratenko.tm.enumerated.Status;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;
import ru.kondratenko.tm.service.ProjectTaskService;
import ru.kondratenko.tm.service.TaskService;
import ru.kondratenko.tm.service.UserService;

import javax.jws.WebService;
import java.io.IOException;

@WebService(endpointInterface = "ru.kondratenko.tm.controller.IControllerTask")
public class IControllerTaskImpl implements IControllerTask {
    private final TaskService taskService;

    private final UserService userService;

    private final ProjectTaskService projectTaskService;

    public IControllerTaskImpl() {
        this.userService = UserService.getInstance();
        this.taskService = TaskService.getInstance();
        this.projectTaskService = ProjectTaskService.getInstance();
    }

    @Override
    public TaskResponseDTO create(final TaskDTO task) {
        if(userService.getCurrentUser() != null){
            task.setUserId(userService.getCurrentUser().getId());
        }
        return taskService.create(task);
    }

    @Override
    public TaskResponseDTO updateByIndex(final int index, TaskDTO task) throws TaskNotFoundException {
        return taskService.updateByIndex(index,task);
    }

    @Override
    public TaskResponseDTO updateById(final long id, TaskDTO task) throws TaskNotFoundException {
        return taskService.updateById(id,task);
    }

    @Override
    public TaskResponseDTO clear() throws TaskNotFoundException {
        if (userService.getCurrentUser() == null) {
            taskService.clear();
        } else {
            for(TaskDTO task: taskService.findAllByUserId(userService.getCurrentUser().getId()).getPayloadTask()) {
                taskService.removeById(task.getId());
            }
        }
        return TaskResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public TaskResponseDTO removeByName(final String name) throws TaskNotFoundException {
        taskService.removeByName(name);
        return TaskResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public TaskResponseDTO removeById(final long id) throws TaskNotFoundException {
        return  taskService.removeById(id);
    }

    @Override
    public TaskResponseDTO removeByIndex(final int index) throws TaskNotFoundException {
        return  taskService.removeByIndex(index);
    }

    @Override
    public TaskResponseDTO viewById(final Long id) throws TaskNotFoundException {
        return taskService.findById(id);
    }

    @Override
    public TaskResponseDTO viewByIndex(final int index) throws TaskNotFoundException {
        return taskService.findByIndex(index);
    }

    @Override
    public ListTaskResponseDTO viewByName(final String name) throws TaskNotFoundException {
        return taskService.findByName(name);
    }

    @Override
    public ListTaskResponseDTO list() {
        ListTaskResponseDTO projectList;
        if (userService.getCurrentUser() == null) {
            projectList = taskService.findAll();
        } else {
            projectList = taskService.findAllByUserId(userService.getCurrentUser().getId());
        }
        return projectList;

    }

    @Override
    public ListTaskResponseDTO listTaskByProjectId(final long projectId) {
        return taskService.findAllByProjectId(projectId);
    }

    @Override
    public TaskResponseDTO addTaskToProjectByIds(final long projectId, final long taskId) throws ProjectNotFoundException, TaskNotFoundException {
        return projectTaskService.addTaskToProject(projectId, taskId);
    }

    @Override
    public TaskResponseDTO removeTaskFromProjectByIds(final long projectId,final long taskId) {
        return  projectTaskService.removeTaskFromProject(projectId, taskId);
    }

    @Override
    public TaskResponseDTO saveJSON(final String  fileName) throws IOException {
        if (fileName == null|| fileName.isEmpty()) return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
        taskService.saveJSON(fileName);
        return TaskResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public TaskResponseDTO saveXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
        taskService.saveXML(fileName);
        return TaskResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public TaskResponseDTO uploadFromJSON(final String  fileName) throws IOException {
        if (fileName == null|| fileName.isEmpty()) return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
        taskService.uploadFromJSON(fileName);
        return TaskResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public TaskResponseDTO uploadFromXML(final String  fileName) throws IOException {
        if (fileName == null|| fileName.isEmpty()) return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
        taskService.uploadFromXML(fileName);
        return TaskResponseDTO.builder().status(Status.OK).build();
    }
}

