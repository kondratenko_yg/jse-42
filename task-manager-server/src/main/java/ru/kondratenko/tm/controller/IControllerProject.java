package ru.kondratenko.tm.controller;

import ru.kondratenko.tm.dto.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.ProjectResponseDTO;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.exception.NotFoundException;
import ru.kondratenko.tm.exception.ProjectNotFoundException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.IOException;

@WebService
@SOAPBinding(
        style = SOAPBinding.Style.RPC
)
public interface IControllerProject  {
    @WebMethod
    ProjectResponseDTO create(final Project project);

    @WebMethod
    ProjectResponseDTO updateByIndex(@WebParam(name = "index") final int index, final Project project) throws ProjectNotFoundException;

    @WebMethod
    ProjectResponseDTO updateById(@WebParam(name = "id") final long id, final Project project) throws ProjectNotFoundException;

    @WebMethod
    ListProjectResponseDTO viewByName(@WebParam(name = "name") final String name) throws ProjectNotFoundException;
    @WebMethod
    ProjectResponseDTO viewById( @WebParam(name = "id") final Long id) throws NotFoundException;

    @WebMethod
    ProjectResponseDTO viewByIndex(@WebParam(name = "index") final int index) throws NotFoundException;

    @WebMethod
    ProjectResponseDTO removeByIndex(@WebParam(name = "index") final int index) throws NotFoundException;

    @WebMethod
    ProjectResponseDTO removeById(@WebParam(name = "id") final long id) throws NotFoundException;

    @WebMethod
    ProjectResponseDTO removeByName(@WebParam(name = "name") final String name) throws NotFoundException;

    @WebMethod
    ProjectResponseDTO clear() throws NotFoundException;

    @WebMethod
    ListProjectResponseDTO list();

    @WebMethod
    ProjectResponseDTO saveJSON(@WebParam(name = "fileName") final String fileName) throws IOException;

    @WebMethod
    ProjectResponseDTO saveXML(@WebParam(name = "fileName") final String fileName) throws IOException;

    @WebMethod
    ProjectResponseDTO uploadFromJSON(@WebParam(name = "fileName") final String fileName) throws IOException;

    @WebMethod
    ProjectResponseDTO uploadFromXML(@WebParam(name = "fileName") final String fileName) throws IOException;

}
