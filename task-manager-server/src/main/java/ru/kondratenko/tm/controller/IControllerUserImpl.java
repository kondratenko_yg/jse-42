package ru.kondratenko.tm.controller;

import lombok.var;
import ru.kondratenko.tm.dto.ListUserResponseDTO;
import ru.kondratenko.tm.dto.UserResponseDTO;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Status;
import ru.kondratenko.tm.exception.UserNotFoundException;
import ru.kondratenko.tm.service.UserService;

import javax.jws.WebService;
import java.io.IOException;
import java.util.Optional;

@WebService(endpointInterface = "ru.kondratenko.tm.controller.IControllerUser")
public class IControllerUserImpl implements IControllerUser {
    private final UserService userService = UserService.getInstance();

    @Override
    public UserResponseDTO create(final User user) {
        return userService.create(user);
    }

    @Override
    public UserResponseDTO updateByIndex(final int index, final User user)throws UserNotFoundException {
        return userService.updateByIndex(index,user);
    }

    @Override
    public UserResponseDTO updateById(final long id, final User user)throws UserNotFoundException {
        return userService.updateById(id,user);
    }

    @Override
    public UserResponseDTO updateByName(final String name, final User user)throws UserNotFoundException {
        return userService.updateByName(name,user);
    }

    @Override
    public UserResponseDTO viewById(final Long id) throws UserNotFoundException {
        return userService.findById(id);
    }

    @Override
    public UserResponseDTO viewByIndex(final int index) throws UserNotFoundException {
        return userService.findByIndex(index);
    }

    @Override
    public UserResponseDTO viewByName(final String name) throws UserNotFoundException {
        return userService.findByName(name);
    }

    @Override
    public UserResponseDTO clear() {
        userService.clear();
        return UserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public UserResponseDTO removeByName(final String name) throws UserNotFoundException {
        return userService.removeByName(name);
    }

    @Override
    public UserResponseDTO removeByIndex(final int index) throws UserNotFoundException {
        return userService.removeByIndex(index);
    }

    @Override
    public UserResponseDTO removeById(final long id) throws UserNotFoundException {
        return userService.removeById(id);
    }

    @Override
    public ListUserResponseDTO list() {
        return userService.findAll();
    }

    @Override
    public UserResponseDTO displayUserInfo() throws UserNotFoundException {
        if(userService.getCurrentUser() != null){
            return viewById(userService.getCurrentUser().getId());
        }
        throw new UserNotFoundException("Registry.");
    }

    @Override
    public UserResponseDTO registry(final String name, String password) throws UserNotFoundException {
        final var user = userService.findByName(name);
        if (user.getStatus() == Status.DB_ERROR) {
            return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        if (userService.checkPassword(Optional.of(user.getPayloadUser()), password)) {
            userService.setCurrentUser(user.getPayloadUser());
            return UserResponseDTO.builder().status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO logOff() {
        userService.setCurrentUser(null);
        return UserResponseDTO.builder().status(Status.OK).build();
    }


    @Override
    public UserResponseDTO saveJSON(final String  fileName) throws IOException {
        if (fileName == null|| fileName.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        userService.saveJSON(fileName);
        return UserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public UserResponseDTO saveXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        userService.saveXML(fileName);
        return UserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public UserResponseDTO uploadFromJSON(final String  fileName) throws IOException {
        if (fileName == null|| fileName.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        userService.uploadFromJSON(fileName);
        return UserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public UserResponseDTO uploadFromXML(final String  fileName) throws IOException {
        if (fileName == null|| fileName.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        userService.uploadFromXML(fileName);
        return UserResponseDTO.builder().status(Status.OK).build();
    }
}
