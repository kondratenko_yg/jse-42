package ru.kondratenko.tm.controller;

import ru.kondratenko.tm.dto.ListTaskResponseDTO;
import ru.kondratenko.tm.dto.TaskDTO;
import ru.kondratenko.tm.dto.TaskResponseDTO;
import ru.kondratenko.tm.exception.NotFoundException;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.IOException;

@WebService
@SOAPBinding(
        style = SOAPBinding.Style.RPC
)
public interface IControllerTask {
    @WebMethod
    TaskResponseDTO create(final TaskDTO task);

    @WebMethod
    TaskResponseDTO updateById(@WebParam(name = "id") final long id, final TaskDTO task) throws TaskNotFoundException;

    @WebMethod
    TaskResponseDTO updateByIndex(@WebParam(name = "index") final int index, TaskDTO task) throws TaskNotFoundException;

    @WebMethod
    ListTaskResponseDTO listTaskByProjectId(@WebParam(name = "id") final long projectId);

    @WebMethod
    TaskResponseDTO addTaskToProjectByIds(@WebParam(name = "projectId") final long projectId, @WebParam(name = "taskId") final long taskId) throws ProjectNotFoundException, TaskNotFoundException;

    @WebMethod
    TaskResponseDTO removeTaskFromProjectByIds(@WebParam(name = "projectId") final long projectId, @WebParam(name = "taskId") final long taskId);

    @WebMethod
    ListTaskResponseDTO viewByName(@WebParam(name = "name") final String name) throws TaskNotFoundException;

    @WebMethod
    TaskResponseDTO viewById( @WebParam(name = "id") final Long id) throws NotFoundException;

    @WebMethod
    TaskResponseDTO viewByIndex(@WebParam(name = "index") final int index) throws NotFoundException;

    @WebMethod
    TaskResponseDTO removeByIndex(@WebParam(name = "index") final int index) throws NotFoundException;

    @WebMethod
    TaskResponseDTO removeById(@WebParam(name = "id") final long id) throws NotFoundException;

    @WebMethod
    TaskResponseDTO removeByName(@WebParam(name = "name") final String name) throws NotFoundException;

    @WebMethod
    TaskResponseDTO clear() throws NotFoundException;

    @WebMethod
    ListTaskResponseDTO list();

    @WebMethod
    TaskResponseDTO saveJSON(@WebParam(name = "fileName") final String fileName) throws IOException;

    @WebMethod
    TaskResponseDTO saveXML(@WebParam(name = "fileName") final String fileName) throws IOException;

    @WebMethod
    TaskResponseDTO uploadFromJSON(@WebParam(name = "fileName") final String fileName) throws IOException;

    @WebMethod
    TaskResponseDTO uploadFromXML(@WebParam(name = "fileName") final String fileName) throws IOException;
}
