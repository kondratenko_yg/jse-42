package ru.kondratenko.tm;

import ru.kondratenko.tm.controller.IControllerProjectImpl;
import ru.kondratenko.tm.controller.IControllerTaskImpl;
import ru.kondratenko.tm.controller.IControllerUserImpl;

import javax.xml.ws.Endpoint;

public class Application {

    public static void main(final String[] args) {
        IControllerProjectImpl projectController = new IControllerProjectImpl();
        Endpoint.publish("http://localhost:8899/ws/project", projectController);

        IControllerTaskImpl taskController = new IControllerTaskImpl();
        Endpoint.publish("http://localhost:8899/ws/task", taskController);

        IControllerUserImpl userController = new IControllerUserImpl();
        Endpoint.publish("http://localhost:8899/ws/user", userController);

    }

}
