package ru.kondratenko.tm.repository;

import ru.kondratenko.tm.entity.AbstractEntity;
import ru.kondratenko.tm.repository.datasource.PooledDataSource;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    Optional<E> create(final E item);

    List<E> findAll();

    List<E> clear();

    Optional<E> findByIndex(final int index);

    Optional<E> findById(final Long id);

    void removeByIndex(final int index);

    void removeById(final Long id);

    Optional<E> update(final E project1);
}
