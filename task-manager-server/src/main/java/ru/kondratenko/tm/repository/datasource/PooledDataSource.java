package ru.kondratenko.tm.repository.datasource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PooledDataSource {
    private static ComboPooledDataSource comboPooledDataSource;

    static {
        comboPooledDataSource = new ComboPooledDataSource();
        Properties properties = new Properties();
        try (InputStream inputStream = new FileInputStream("C:\\Users\\Lenovo\\Documents\\db.properties.txt")) {
            properties.load(inputStream);
            comboPooledDataSource.setDriverClass(properties.getProperty("DRIVER_CLASS"));
            comboPooledDataSource.setJdbcUrl(properties.getProperty("DB_CONNECTION_URL"));
            comboPooledDataSource.setUser(properties.getProperty("DB_USER"));
            comboPooledDataSource.setPassword(properties.getProperty("DB_PWD"));
            comboPooledDataSource.setInitialPoolSize(2);
            comboPooledDataSource.setMinPoolSize(2);
            comboPooledDataSource.setAcquireIncrement(1);
            comboPooledDataSource.setMaxPoolSize(5);
        }catch (PropertyVetoException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static DataSource getDataSource() {
        return comboPooledDataSource;
    }
}
