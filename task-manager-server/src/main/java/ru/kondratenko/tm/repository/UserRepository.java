package ru.kondratenko.tm.repository;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.repository.datasource.PooledDataSource;
import ru.kondratenko.tm.repository.mapper.ExecuteHelper;
import ru.kondratenko.tm.repository.mapper.UserMapper;

import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class UserRepository implements IUserRepository  {
    public User currentUser;

    private static volatile UserRepository instance;

    private ExecuteHelper<User> executeHelper = new ExecuteHelper<>(User.class);

    private UserRepository() {
    }

    public static UserRepository getInstance(){
        UserRepository localInstance = instance;
        if (localInstance == null) {
            synchronized (UserRepository.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new UserRepository();
                }
            }
        }
        return localInstance;
    }

    @Override
    public Optional<User> create(User input) {
        String sql = "insert into users(name, password, firstName, lastName, role) values (?, ?, ?, ?, ?)";
        return executeHelper.executeInsert(input,sql);
    }

    @Override
    public Optional<User> update(User input) {
        String sql = "update users set name = ?, password = ?, firstName = ?, lastName = ?, role = ? where id = ?";
        return executeHelper.executeUpdate(input,sql,6);
    }

    @Override
    public Optional<User> findById(Long id) {
        String sql = "select * from users where id = ?";
        Object[] parameters = new Object[] {id};
        return executeHelper.execute(parameters,sql);
    }

    @Override
    public void removeById(Long id){
        String sql = "delete from users where id = ?";
        Object[] parameters = new Object[] {id};
        executeHelper.execute(parameters,sql);
    }

    @Override
    public Optional<User> findByIndex(int index)  {
        String sql = "select* from(select *, row_number()  OVER () as rnum from users) as f where rnum=?";
        Object[] parameters = new Object[] {index};
        return executeHelper.execute(parameters,sql);
    }

    @Override
    public void removeByIndex(int index)  {
        String sql = "delete from users where id in (select id from (select users.*,row_number()  OVER () as rnum from users) as f where rnum =?)";
        Object[] parameters = new Object[] {index};
        executeHelper.execute(parameters,sql);
    }

    @Override
    public Optional<User> findByName(String name){
        String sql = "select* from users where name=?";
        Object[] parameters = new Object[] {name};
        return executeHelper.execute(parameters,sql);
    }

    @Override
    public void removeByName(String name)  {
        String sql = "delete from users where name=?";
        Object[] parameters = new Object[] {name};
        executeHelper.execute(parameters,sql);
    }

    @Override
    public List<User> findAll() {
        String sql = "select * from users";
        return executeHelper.execute(sql);
    }

    @Override
    public List<User> clear() {
        String sql = "delete from users";
        return executeHelper.execute(sql);
    }
}
