package ru.kondratenko.tm.repository;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.kondratenko.tm.entity.AbstractEntity;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public interface ITaskProjectRepository<E extends AbstractEntity> extends IRepository<E> {
    List<E> findAllByUserId(final Long userId);
    void removeByName(final String name);
    List<E> findByName(final String name);
}
