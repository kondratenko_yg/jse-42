package ru.kondratenko.tm.repository.mapper;

import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public  class UserMapper extends ExecuteHelper<User> {

    public UserMapper(Class<User> type) {
        super(type);
    }

    public User getItemFromResultSet(ResultSet resultSet) throws SQLException {
        return User.builder()
                .id(resultSet.getLong("id"))
                .name(resultSet.getString("name"))
                .password(resultSet.getString("password"))
                .firstName(resultSet.getString("firstName"))
                .lastName(resultSet.getString("lastName"))
                .role(Role.USER)
                .build();
    }

    public void setupStatement(PreparedStatement preparedStatement, User input) throws SQLException {
        preparedStatement.setString(1, input.getName());
        preparedStatement.setString(2, input.getPassword());
        preparedStatement.setString(3, input.getFirstName());
        preparedStatement.setString(4, input.getLastName());
        preparedStatement.setString(5, input.getRole() == null ? Role.USER.getDisplayName() : input.getRole().getDisplayName());
    }
}
