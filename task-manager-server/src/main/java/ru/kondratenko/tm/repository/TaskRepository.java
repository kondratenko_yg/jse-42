package ru.kondratenko.tm.repository;

import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.repository.datasource.PooledDataSource;
import ru.kondratenko.tm.repository.mapper.ExecuteHelper;
import ru.kondratenko.tm.repository.mapper.TaskMapper;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class TaskRepository  implements ITaskRepository  {

    private static volatile TaskRepository instance ;

    private ExecuteHelper<Task> executeHelper = new ExecuteHelper<>(Task.class);

    private TaskRepository() {
    }

    public static TaskRepository getInstance(){
        TaskRepository localInstance = instance;
        if (localInstance == null) {
            synchronized (TaskRepository.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new TaskRepository();
                }
            }
        }
        return localInstance;
    }

    @Override
    public Optional<Task> create(Task input) {
        String sql = "insert into tasks(name, description, projectId, userId, deadline) values (?, ?, ?,?,?)";
        return executeHelper.executeInsert(input,sql);
    }

    @Override
    public Optional<Task> update(Task input) {
        String sql = "update tasks set name = ?, description = ?,projectId = ?, userId = ?,deadline = ? where id = ?";
        return executeHelper.executeUpdate(input,sql,6);
    }

    @Override
    public Optional<Task> findById(Long id) {
        String sql = "select * from tasks where id = ?";
        Object[] parameters = new Object[] {id};
        return executeHelper.execute(parameters,sql);
    }

    @Override
    public void removeById(Long id){
        String sql = "delete from tasks where id = ?";
        Object[] parameters = new Object[] {id};
        executeHelper.execute(parameters,sql);
    }

    @Override
    public Optional<Task> findByIndex(int index)  {
        String sql = "select* from(select *, row_number()  OVER () as rnum from tasks) as f where rnum=?";
        Object[] parameters = new Object[] {index};
        return executeHelper.execute(parameters,sql);
    }

    @Override
    public void removeByIndex(int index)  {
        String sql = "delete from tasks where id in (select id from (select tasks.*,row_number()  OVER () as rnum from tasks) as f where rnum =?)";
        Object[] parameters = new Object[] {index};
        executeHelper.execute(parameters,sql);
    }

    @Override
    public List<Task> findByName(String name){
        String sql = "select* from tasks where name=?";
        Object[] parameters = new Object[] {name};
        return executeHelper.executeList(parameters,sql);
    }

    @Override
    public void removeByName(String name)  {
        String sql = "delete from tasks where name=?";
        Object[] parameters = new Object[] {name};
        executeHelper.executeList(parameters,sql);
    }

    @Override
    public List<Task> findAll() {
        String sql = "select * from tasks";
        return executeHelper.execute(sql);
    }

    @Override
    public List<Task> findAllByUserId(final Long id) {
        String sql = "select * from tasks where userid=?";
        Object[] parameters = new Object[] {id};
        return executeHelper.executeList(parameters,sql);
    }

    @Override
    public Optional<Task> findByProjectIdAndId(Long projectId, Long id) {
        String sql = "select * from tasks where id = ? and projectid=?";
        Object[] parameters = new Object[] {id,projectId};
        return executeHelper.execute(parameters,sql);
    }

    @Override
    public List<Task> findAllByProjectId(Long projectId) {
        String sql = "select * from tasks where projectid=?";
        Object[] parameters = new Object[] {projectId};
        return executeHelper.executeList(parameters,sql);
    }

    @Override
    public List<Task> clear() {
        String sql = "delete from tasks";
        return executeHelper.execute(sql);
    }
}
