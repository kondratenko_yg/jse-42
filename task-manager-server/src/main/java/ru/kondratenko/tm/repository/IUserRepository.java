package ru.kondratenko.tm.repository;

import ru.kondratenko.tm.entity.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {
    void removeByName(final String login);
    Optional<User> findByName(final String login);
}
