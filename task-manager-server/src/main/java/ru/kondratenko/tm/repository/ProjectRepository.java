package ru.kondratenko.tm.repository;

import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.repository.mapper.ExecuteHelper;

import java.util.List;
import java.util.Optional;

public class ProjectRepository implements ITaskProjectRepository<Project> {

    private static volatile ProjectRepository instance;

    private final ExecuteHelper<Project> executeHelper = new ExecuteHelper<>(Project.class);

    private ProjectRepository() {
    }

    public static ProjectRepository getInstance(){
        ProjectRepository localInstance = instance;
        if (localInstance == null) {
            synchronized (ProjectRepository.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ProjectRepository();
                }
            }
        }
        return localInstance;
    }

    @Override
    public Optional<Project> create(Project input) {
        String sql = "insert into projects(name, description, userId) values (?, ?, ?)";
        return executeHelper.executeInsert(input,sql);
    }

    @Override
    public Optional<Project> update(Project input) {
        String sql = "update projects set name = ?, description = ?, userId = ? where id = ?";
        return executeHelper.executeUpdate(input,sql,4);
    }

    @Override
    public Optional<Project> findById(Long id) {
        String sql = "select * from projects where id = ?";
        Object[] parameters = new Object[] {id};
        return executeHelper.execute(parameters,sql);
    }

    @Override
    public void removeById(Long id){
        String sql = "delete from projects where id = ?";
        Object[] parameters = new Object[] {id};
        executeHelper.execute(parameters,sql);
    }

    @Override
    public Optional<Project> findByIndex(int index)  {
        String sql = "select* from(select *, row_number()  OVER () as rnum from projects) as f where rnum=?";
        Object[] parameters = new Object[] {index};
        return executeHelper.execute(parameters,sql);
    }

    @Override
    public void removeByIndex(int index)  {
        String sql = "delete from projects where id in (select id from (select projects.*,row_number()  OVER () as rnum from projects) as f where rnum =?)";
        Object[] parameters = new Object[] {index};
        executeHelper.execute(parameters,sql);
    }

    @Override
    public List<Project> findByName(String name){
        String sql = "select* from projects where name=?";
        Object[] parameters = new Object[] {name};
        return executeHelper.executeList(parameters,sql);
    }

    @Override
    public void removeByName(String name)  {
        String sql = "delete from projects where name=?";
        Object[] parameters = new Object[] {name};
        executeHelper.executeList(parameters,sql);
    }

    @Override
    public List<Project> findAll() {
        String sql = "select * from projects";
        return executeHelper.execute(sql);
    }

    @Override
    public List<Project> findAllByUserId(final Long id) {
        String sql = "select * from projects where userid=?";
        Object[] parameters = new Object[] {id};
        return executeHelper.executeList(parameters,sql);
    }

    @Override
    public List<Project> clear() {
        String sql = "delete from projects";
        return executeHelper.execute(sql);
    }

}
