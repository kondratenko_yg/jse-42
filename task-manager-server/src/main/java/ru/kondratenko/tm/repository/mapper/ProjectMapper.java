package ru.kondratenko.tm.repository.mapper;

import ru.kondratenko.tm.entity.Project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public  class ProjectMapper extends ExecuteHelper<Project> {

    public ProjectMapper(Class<Project> type) {
        super(type);
    }

    public  Project getItemFromResultSet(ResultSet resultSet) throws SQLException {
        return Project.builder()
                .id(resultSet.getLong("id"))
                .name(resultSet.getString("name"))
                .description(resultSet.getString("description"))
                .userId(resultSet.getLong("userId"))
                .build();
    }

    public void setupStatement(PreparedStatement preparedStatement, Project input) throws SQLException {
        preparedStatement.setString(1, input.getName());
        preparedStatement.setString(2, input.getDescription());
        preparedStatement.setObject(3,  input.getUserId());

    }
}
