package ru.kondratenko.tm.repository.mapper;

import ru.kondratenko.tm.entity.Task;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

public class TaskMapper extends ExecuteHelper<Task> {

    public TaskMapper(Class<Task> type) {
        super(type);
    }

    public  Task getItemFromResultSet(ResultSet resultSet) throws SQLException {
        Task task = Task.builder()
                .id(resultSet.getLong("id"))
                .name(resultSet.getString("name"))
                .description(resultSet.getString("description"))
                .projectId(resultSet.getLong("projectId"))
                .userId(resultSet.getLong("userId"))
                .build();
        try{
            task.setDeadline(LocalDateTime.parse(resultSet.getString("deadline")));
        }
        catch (DateTimeParseException ignored){ }
        return task;
    }

    public void setupStatement(PreparedStatement preparedStatement, Task input) throws SQLException {
        preparedStatement.setString(1, input.getName());
        preparedStatement.setString(2, input.getDescription());
        preparedStatement.setObject(3, input.getProjectId());
        preparedStatement.setObject(4, input.getUserId());
        preparedStatement.setObject(5, String.valueOf(input.getDeadline()));
    }
}
