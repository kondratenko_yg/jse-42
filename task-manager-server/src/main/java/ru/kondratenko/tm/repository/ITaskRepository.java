package ru.kondratenko.tm.repository;

import ru.kondratenko.tm.entity.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends ITaskProjectRepository<Task> {
    Optional<Task> findByProjectIdAndId(final Long projectId, final Long id);
    List<Task> findAllByProjectId(final Long projectId);
}
