package ru.kondratenko.tm.repository.mapper;

import ru.kondratenko.tm.entity.AbstractEntity;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.repository.datasource.PooledDataSource;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ExecuteHelper<E extends AbstractEntity> {

    private final Class<E> type;

    public ExecuteHelper(Class<E> type) {
        this.type = type;
    }

    public Class<E> getMyType() {
        return this.type;
    }

    public <E extends AbstractEntity> ExecuteHelper getMapper() {
        if(getMyType() == Project.class){
            return new ProjectMapper(Project.class);
        }
        if(getMyType() == Task.class){
            return new TaskMapper(Task.class);
        }
        return new UserMapper(User.class);
    }

    public void setParameters(PreparedStatement preparedStatement,Object[] parameters) throws SQLException {
        for (int index = 1; index <= parameters.length; index++) {
            if (parameters[index-1] != null) {
                preparedStatement.setObject(index, parameters[index-1]);
            }
        }
    }

    public Optional<E> execute(Object[] parameters, String sql) {
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                setParameters(preparedStatement,parameters);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    return (Optional<E>) Optional.of(getMapper().getItemFromResultSet(resultSet));
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Optional.empty();
    }

    public List<E> execute(String sql) {
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery(sql);
                List<E> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add((E) getMapper().getItemFromResultSet(resultSet));
                }
                return result;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Collections.emptyList();
    }

    public List<E> executeList(Object[] parameters, String sql){
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                setParameters(preparedStatement,parameters);
                ResultSet resultSet = preparedStatement.executeQuery();
                List<E> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add((E) getMapper().getItemFromResultSet(resultSet));
                }
                return result;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Collections.emptyList();
    }

    public Optional<E> executeInsert(E input, String sql) {
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                getMapper().setupStatement(preparedStatement, input);
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    input.setId(resultSet.getLong(1));
                }
                return Optional.of(input);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Optional.empty();
    }

    public Optional<E> executeUpdate(E input, String sql,int indexId) {
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                getMapper().setupStatement(preparedStatement, input);
                preparedStatement.setLong(indexId, input.getId());
                preparedStatement.executeUpdate();
                return Optional.of(input);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Optional.empty();
    }

    public E getItemFromResultSet(ResultSet resultSet) throws SQLException {
        return null;
    }

    public void setupStatement(PreparedStatement preparedStatement, E input) throws SQLException {
    }

}
