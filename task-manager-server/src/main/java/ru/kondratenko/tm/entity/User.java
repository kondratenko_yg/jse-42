package ru.kondratenko.tm.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kondratenko.tm.enumerated.Role;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
public class User extends AbstractEntity  implements Serializable {
    public static final Long serialVersionUID = 1L;

    private Long id = System.nanoTime();

    private String name = "";

    private String password;

    private String firstName = "";

    private String lastName = "";

    private Role role = Role.USER;

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public User(String name, String password, Role role) {
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public User(Long id,String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public User(String name, String password, String firstName, String lastName, Role role) {
        this.name = name;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

    public User(Long id, String name, String password, String firstName, String lastName, Role role) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
