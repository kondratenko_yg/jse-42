package ru.kondratenko.tm.entity;

import lombok.Data;

@Data
public abstract class AbstractEntity {
    public static final Long serialVersionUID = 1L;

    private Long id;

    private String name;
}
