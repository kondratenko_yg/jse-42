package ru.kondratenko.tm.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
public class Task extends AbstractEntity  implements Serializable {
    public static final Long serialVersionUID = 1L;

    private Long id = System.nanoTime();

    private String name = "";

    private String description = "";

    private Long projectId;

    private Long userId;

    private LocalDateTime deadline  = LocalDateTime.now().plusMinutes(480L);

    public Task(String name) {
        this.name = name;
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
        this.deadline = LocalDateTime.now().plusMinutes(480L);
    }

    public Task(String name, String description, Long userId) {
        this.name = name;
        this.description = description;
        this.userId = userId;
        this.deadline = LocalDateTime.now().plusMinutes(480L);
    }

    public Task(String name, String description, Long userId, Long minutes) {
        this.name = name;
        this.description = description;
        this.userId = userId;
        this.deadline = LocalDateTime.now().plusMinutes(minutes);
    }

    public Task(String name, String description, Long projectId, Long userId, LocalDateTime deadline) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.userId = userId;
        this.deadline = deadline;
    }

    public Task(Long id, String name, String description, Long projectId, Long userId, LocalDateTime deadline) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.userId = userId;
        this.deadline = deadline;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

}
